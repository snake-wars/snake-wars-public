# Snake Wars

In one faraway galaxy...

Two snake warriors, soldiers of fortune travel over ancient lands 
and take part in wars and battles for money.

Leaders, kings and emperors hire them for the sake of victory.
From battle to battle friends-warriors getting stronger, buy new weapon
and gain new abilities.

Their team is becoming larger, legend warriors and heroes join them.
Soon their squad can win any war and rulers respected and scared them.

But over the time they realised some mysterious and dark power
governs ancient kingdoms...


## Lemmy
![lemmy](assets/lemmy.gif)

| Ability |      Power      |
| :---:   | :-------------: |
| Health  | 100             |
| Bite    | 1               |


## Rusty
![lemmy](assets/warry.gif)

| Ability           |      Power      | 
| :---:             | :-------------: |
| Health            | 100             |
| Bite              | 2               |
| Pitchfork Fencing | 2               |
| Fight Tactic      | Rational        |
| Build             | 3               |
| Survive           | 2               |
| Earn Artifacts    | 2               |

## Warry
![lemmy](assets/warry.gif)

| Ability           |      Power      |    Weapon / Armor                   |
| :---:             | :-------------: | :---------------------------------: |
| Health            | 200             |                                     
| Bite              | 2               | Warry Helm                          
| Helm              | +               |![lemmy](assets/warry_helm.png)
| Sword Fencing     | 2               |Wood:  1Level
| Fight Tactic      | Rational        |
| Defence territory | 3               | Warry Sword
| Bodyguard         | 3               |![lemmy](assets/warry_sword.png)
| Chaser            | 2               |Handle: Wood 1Level
| Survive           | 3               |Blade: Bronze 1Level
| Earn Artifacts    | 3               |


## Vikky
![lemmy](assets/vikky.gif)

| Ability           |      Power      |      Weapon / Armor                 | 
| :---:             | :-------------: | :---------------------------------: |
| Health            | 300             |
| Bite              | 3               | Vikky Helm
| Helm              | +               |![lemmy](assets/vikky_helm_64.png)
| Fight Tactic      | Rational        |Metal: 1Level 
| Shield            | +               |Wood:  3Level
| Bodyguard         | 5               |
| Axe Fencing       | 3               | Vikky Shield
| Defence territory | 5               |![lemmy](assets/vikky_shield.png)
| Chaser            | 5               |Wood:  3Level
| Survive           | 3               |
| Earn Artifacts    | 3               | Lite Axe
|                   |                 |![lemmy](assets/vikky_axe_64.png)
|                   |                 |Handle: Wood 3Level
|                   |                 |Blade: Bronze 2Level
|                   |                 |
|                   |                 |


## Bersy
![lemmy](assets/bersy.gif)

| Ability           |      Power      |      Weapon / Armor                 |  
| :---:             | :-------------: | :---------------------------------: |
| Health            | 1000            |
| Bite              | 10              | Magic Bear Head
| Helm              | +               |![lemmy](assets/bear_head.png)
| Shield            | +               |
| Axe Fencing       | 7               | Bersy Shield
| Sword Fencing     | 7               |![lemmy](assets/bersy_shield.png)
| Spear Fencing     | 7               | Steel:  7Level
| Both Hands Fencing| +               | Wood:  7Level
| Weapon Throwing   | 7               |
| Direct Rage       | 7               | Heavy Axe
| Rage Hook Left    | 7               |![lemmy](assets/heavy_axe.png)
| Rage Hook Right   | 7               |Handle: Wood 7Level
| Side Rage Left    | 7               |Blade: Steel 7Level
| Side Rage Right   | 7               |
| Fight Tactic      | Mad             | Spear
| Defence territory | 2               |![lemmy](assets/spear.png)
| Bodyguard         | 7               |Handle: Wood 5Level
| Chaser            | 7               |Blade: Bronze 5Level
| Survive           | 1               |
| Earn Artifacts    | 1               |


## Ulfy
![lemmy](assets/ulfy.png)

| Ability           |      Power           |       Weapon / Armor                |  
| :---:             | :-------------:      | :---------------------------------: |
| Health            | 700                  |
| Bite              | 5                    | Magic Wolf Head
| Weapon Throwing   | 7                    |![lemmy](assets/wolfy_head.png)
| Bomb Throwing     | 7                    |
| Fight Tactic      | Canny, from distance | Bomb Pouch
| Defence territory | 2                    |![lemmy](assets/bag.png)
| Bodyguard         | 2                    |3 Level: Capacity 3 Bombs
| Chaser            | 7                    |
| Survive           | 7                    | Magic Pouch
| Earn Artifacts    | 7                    |![lemmy](assets/glass.png)
|                   |                      |3 Level: Capacity 3 Magics
|                   |                      |
|                   |                      |
|                   |                      |
|                   |                      |

## Choky
![lemmy](assets/bersy.png)

| Ability           |      Power      |      Weapon / Armor                 |  
| :---:             | :-------------: | :---------------------------------: |
| Health            | 1000            |  
| Bite              | 5               | 
| Health            | 1000            |
| Health            | 1000            |
| Health            | 1000            |
| Health            | 1000            |

## Rippy
![lemmy](assets/rippy.png)

| Ability           |      Power      |      Weapon / Armor                 |  
| :---:             | :-------------: | :---------------------------------: |
| Health            | 3000            |
| Bite              | 5               | Steel Crushing Armor
| Health            | 1000            |![lemmy](assets/rippy_armor.png)
| Health            | 1000            |Steel: 5Level
| Health            | 1000            |
| Health            | 1000            | Steel Mace
| Health            | 1000            |![lemmy](assets/mace.png)
| Health            | 1000            |Chain: Steel 3Level
| Health            | 1000            | Mace: Steal 5Level

# LEVELS

## Lemmy Village

![lemmy](assets/lemmy_village.jpg)

## First Scramble

![lemmy](assets/first_scrumble.jpg)

